const Resume = {
	"header": {
		"name": "Thorsten OJE",
		"email": "ooo.oje@oje.ooo",
		"phone": "06261/8995117",
		"github": "https://github.com/xxxxxxx",
		"linkedin": "https://linkedin.com/in/thorsten-oje",
		"address": "Im Luss 40a",
		"website": "https://ooo.ejo.ooo-training-ooo.oje.ooo/",
		"zip": "74847 Obrigheim",
		"city": "",
		"state": "BW",
		"country": "Germany"
	},
	"experience": [
		{
			"company": "Horn GbR",
			"city": "Obrigheim",
			"state": "BW",
			"position": "Umschüler",
			"dateFrom": "09/2018",
			"dateTo": "02/2019",
			"primaryWorkBrief": "Umschulung zum Landwirt.",
			"impact1": "Musste leider vorzeitig beendet werden.",
			"impact2": "",
			"impact3": "",
			"impact4": "",
			"isVisible": true
		},
		{
			"company": "Deutsche Bank AG",
			"city": "Eschborn",
			"state": "HE",
			"position": "Spezialist",
			"dateFrom": "10/1996",
			"dateTo": "09/2018",
			"primaryWorkBrief": "Tätigkeit im effektiven Bereich der Wertpapierabwicklung.",
			"impact1": "Vault Support - Unterstützende Tätigkeiten für den effektiven Bereich (Reklamationen, Testing, Implementierungen, Verbesserungen).",
			"impact2": "Effektenkasse - Bearbeitung von effektiv abgerechneten Wertpapiergeschäften.",
			"impact3": "Ansprechpartner bei PC-Problemen, bevor der IT-Support der Deutschen Bank AG eingeschaltet wird.",
			"impact4": "Quereinstieg als Sachbearbeiter im effektiven Bereich (Tresor)",
			"isVisible": true
		},
		{
			"company": "Elektro Leonhardt",
			"city": "Dreieich / Sprendlingen",
			"state": "HE",
			"position": "Lehrling",
			"dateFrom": "08/1992",
			"dateTo": "06/1996",
			"primaryWorkBrief": "Ausbildung zum Elektroninstallateur (heute: Elektroniker für Energie-/ und Gebäudetechnik)",
			"impact1": "4 von 44 haben die Abschlussprüfung bestanden. Ihr Bewerber, als bester.",
			"impact2": "Theorie 4 - Gesellenstück 2.",
			"impact3": "",
			"impact4": "",
			"isVisible": true
		}
	],
	"education": [
		{
			"site": "August-Bebel-Schule",
			"dateFrom": "08/1992",
			"dateTo": "01/1996",
			"studyDegree": "Elektroinstallteur Geselle",
			"isVisible": true
		},
		{
			"site": "Weibelfeldschule",
			"dateFrom": "10/1988",
			"dateTo": "09/1992",
			"studyDegree": "Hauptschulabschluss",
			"isVisible": true
		}
	],
	"technicalSkills": [
		{
			"category": "Betriebssysteme / Virtualisierung",
			"keywords": [
				{
					"name": "MS Server",
					"level": 4
				},
				{
					"name": "Linux",
					"level": 4
				},
				{
					"name": "CoreOS",
					"level": 3
				},
				{
					"name": "Hyper-V",
					"level": 4
				},
				{
					"name": "KVM",
					"level": 4
				},
				{
					"name": "VMWare",
					"level": 3
				},
				{
					"name": "VM Server für x86",
					"level": 2
				},
				{
					"name": "Proxmox",
					"level": 3
				},
				{
					"name": "Docker",
					"level": 4
				},
				{
					"name": "Kubernetes",
					"level": 2
				}
			],
			"isVisible": true
		},
		{
			"category": "Entwickler Sprachen",
			"keywords": [
				{
					"name": "JavaScript",
					"level": 3
				},
				{
					"name": "HTML",
					"level": 4
				},
				{
					"name": "CSS",
					"level": 4
				},
				{
					"name": "Go",
					"level": 2
				},
				{
					"name": "Python3",
					"level": 2
				}
			],
			"isVisible": true
		},
		{
			"category": "Technologien",
			"keywords": [
				{
					"name": "Nginx",
					"level": 4
				},
				{
					"name": "Apache2",
					"level": 3
				},
				{
					"name": "MySQL",
					"level": 3
				},
				{
					"name": "PostgreSQL",
					"level": 3
				},
				{
					"name": "MongoDB",
					"level": 2
				},
				{
					"name": "Express",
					"level": 3
				},
				{
					"name": "React",
					"level": 3
				},
				{
					"name": "Node.js",
					"level": 3
				},
				{
					"name": "Vue.js",
					"level": 3
				},
				{
					"name": "Redux",
					"level": 2
				},
				{
					"name": "Git",
					"level": 4
				},
				{
					"name": "GitHub",
					"level": 3
				},
				{
					"name": "Ghost CMS",
					"level": 4
				},
				{
					"name": "Workpress",
					"level": 3
				},
				{
					"name": "Postfix",
					"level": 4
				}
			],
			"isVisible": true
		}
	],
	"projects": [
		{
			"name": "Absolvierte LinkedIn LernPfade",
			"dateFrom": "07/2020",
			"dateTo": "09/2020",
			"link": "https://ooo.ejo.ooo-training-ooo.oje.ooo/category/certs/",
			"teamBrief": "Persönliche Weiterbildung",
			"details": [
				"Technologien für die digitale Transformation",
				"Graphikdesigner",
				"JavaScript Programmierer",
				"Full-Stack-Webentwickler",
				"Softwareentwickler",
				"Systemadministrator",
				"Projektmanager",
				"Windows-Support-Techniker"
			],
			"isVisible": true
		},
		{
			"name": "Linux Systemadministrator LPI",
			"link": "https://ooo.ejo.ooo-training-ooo.oje.ooo/2016/05/26/fernstudium-linux-administrator-lpi/",
			"dateFrom": "09/2015",
			"dateTo": "05/2016",
			"teamBrief": "Fernstudium",
			"details": [
				"Note: gut"
			],
			"isVisible": true
		},
		{
			"name": "CloudComputing",
			"link": "https://ooo.ejo.ooo-training-ooo.oje.ooo/2014/08/05/fernstudium-cloud-computing/",
			"dateFrom": "11/2013",
			"dateTo": "08/2014",
			"teamBrief": "Fernstudium",
			"details": [
				"Note: sehr gut"
			],
			"isVisible": true
		}
	],
	"certification": [
		{
			"issuedBy": "Issuer 1/Cert Name",
			"id": "#12345",
			"dateFrom": "XX/XXXX",
			"dateTo": "",
			"isVisible": true
		},
		{
			"issuedBy": "Issuer 2/Cert Name",
			"id": "#12345",
			"dateFrom": "XX/XXXX",
			"dateTo": "XX/XXXX",
			"isVisible": true
		},
		{
			"issuedBy": "Issuer 3/Cert Name",
			"id": "#12345",
			"dateFrom": "",
			"dateTo": "",
			"isVisible": true
		}
	]
}

export default Resume;