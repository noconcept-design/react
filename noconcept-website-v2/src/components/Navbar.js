// rfce
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { FaToriiGate,FaBars, FaTimes } from 'react-icons/fa';
import { IconContext } from 'react-icons/lib'
//import { Button } from './Button';
import './Navbar.css';

function Navbar() {

  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false)

  // responsive
  const showButton = () => {
    if(window.innerWidth <= 960) {
      setButton(false)
    } else {
      setButton(true)
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener('resize', showButton)

  return (
    <>
      <IconContext.Provider value={{ color: '#f00946' }}>
        <div className='navbar'>
          <div className='navbar-container container'>
            <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
            <FaToriiGate className='navbar-icon' />
              NOCONCEPT
            </Link>
            <div className='menu-icon' onClick={handleClick}>
              {click ? <FaTimes /> : <FaBars />}
            </div>
            <ul className={click ? 'nav-menu active' : 'nav-menu'}>
              <li className='nav-item'>
                <Link to='/' className='nav-links' onClick={closeMobileMenu}>Start</Link>
              </li>
              <li className='nav-item'>
                <Link to='/skills' className='nav-links' onClick={closeMobileMenu}>Service</Link>
              </li>
              <li className='nav-item'>
                <Link to='/work' className='nav-links' onClick={closeMobileMenu}>Arbeiten</Link>
              </li>
              <li className='nav-item'>
                <Link to='/gol' className='nav-links' onClick={closeMobileMenu}>Game of Life</Link>
              </li>
              <li className='nav-item'>
                <Link to='/tetris' className='nav-links' onClick={closeMobileMenu}>Tetris</Link>
              </li>
              <li className='nav-item'>
                <Link to='/catmeme' className='nav-links' onClick={closeMobileMenu}>Katzen Meme</Link>
              </li>
              <li className='nav-item'>
                <Link to='/meinungen' className='nav-links' onClick={closeMobileMenu}>Meinungen</Link>
              </li>
                           
              <li className='nav-btn'>
                {/*button ? (
                  <Link to='anmelden' className='btn-link'>
                    <Button buttonStyle='btn--outline'>Anmelden</Button>
                  </Link>
                ): (
                  <Link to='anmelden' className='btn-link'>
                    <Button buttonStyle='btn--outline'
                            buttonSize='btn--mobile'
                            onClick={closeMobileMenu}
                            >Anmelden
                    </Button>
                  </Link>
                )*/}
              </li>
            </ul>
          </div>
        </div>
      </IconContext.Provider>
    </>
  );
}


export default Navbar;
