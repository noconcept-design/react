export const homeObjOne = {
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: 'NOCONCEPT Design',
  headline: 'Wir können',
  description: 'mehr als nur verwackelte Bilder.',
  buttonLabel: 'babbel ned... zeisch!',
  imgStart: '',
  img: 'images/window.png',
  alt: 'Ein Fenster in badisch Lappland'
};

export const homeObjTwo = {
  lightBg: true,
  lightText: false,
  lightTextDesc: false,
  topLine: 'Leitbild',
  headline: 'Unser "<s>Knecht</s>" Azubi versteht sein Handwerk.',
  description: 'Darauf legen wir größten wert!',
  buttonLabel: '',
  imgStart: '',
  img: '',
  alt: ''
};