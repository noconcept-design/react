import React, { Component } from 'react'
import axios from 'axios';
import { FaThumbsDown } from 'react-icons/fa';

class Upload extends Component {
  state = {
    selectedFile: null
  }
  fileSelectedHandler = event => {
    this.setState({
      selectedFile: event.target.files[0]
    })
  }

  fileUploadHandler = () => {
    const fd = new FormData();
    fd.append('image', this.state.selectedFile, this.state.selectedFile.name)
    // URL eintragen
    axios.post('url', fd, {
      onUploadProgress: progressEvent => {
        console.log('Upload Progress: ' + Math.round(progressEvent.loaded / progressEvent.total * 100 ) * '%')
      }
    })
    .then(res => {
      console.log(res);
    });
  };

  render() {
    return (
      <div className="Upload">
      <input
        style={{ display: 'none' }} 
        type='file' 
        onChange={this.fileSelectedHandler} 
        ref={fileInput => this.fileInput = fileInput} />
      <button onClick={() => this.fileInput.click()}>Muschi auswählen</button>
      <button onClick={this.fileUploadHandler}>Hochladen</button>
      </div>
    );
  }
}

export default Upload
