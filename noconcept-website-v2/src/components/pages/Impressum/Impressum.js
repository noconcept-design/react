// rfce
import React from 'react';
import '../../../assets/main.css';

function Impressum() {
  return (
    <>
      <div className='container mx-auto px-4 mt-6'>
        <h1 className='text-xl font-bold'>Impressum</h1>
        <h2 className='text-lg font-hairline'>Diensteanbieter</h2>
        <p className='text-base'>Thorsten OJE</p>
        <p className='text-base'>Im Luss 40a</p>
        <p className='text-base'>74847 Obrigheim</p>
        <p className='text-base'>Deutschland</p>
        </div>
        <div className='container mx-auto px-4 mt-6'>        
        <h2 className='text-lg font-bold'>Kontaktmöglichkeiten</h2>
        <p className='text-base'><strong>E-Mail-Adresse:</strong> <a href='mailto:contact@noconcept.design'>contact@noconcept.design</a></p>
        <p className='text-base'><strong>Kontaktformular:</strong> <a href='https://noconcept.design/contact' target='_blank' rel='noopener noreferrer'>https://noconcept.design/contact</a></p>
        </div>
        <div className='container mx-auto px-4 mt-6'>
        <h2 className='text-lg font-bold'>Haftungs- und Schutzrechtshinweise</h2>
        <p className='text-base'><strong>Haftungsausschluss</strong>: Die Inhalte dieses Onlineangebotes wurden sorgfältig und nach unserem aktuellen Kenntnisstand erstellt, dienen jedoch nur der Information und entfalten keine rechtlich bindende Wirkung, sofern es sich nicht um gesetzlich verpflichtende Informationen (z.B. das Impressum, die Datenschutzerklärung, AGB oder verpflichtende Belehrungen von Verbrauchern) handelt. Wir behalten uns vor, die Inhalte vollständig oder teilweise zu ändern oder zu löschen, soweit vertragliche Verpflichtungen unberührt bleiben. Alle Angebote sind freibleibend und unverbindlich. </p>
        </div>
        <div className='container mx-auto px-4 mt-6'>
        <p className='text-base'><strong>Links auf fremde Webseiten</strong>: Inhalte fremder Webseiten, auf die wir direkt oder indirekt verweisen, liegen außerhalb unseres Verantwortungsbereiches und machen wir uns nicht zu Eigen. Für alle Inhalte und insbesondere für Schäden, die aus der Nutzung der in den verlinkten Webseiten aufrufbaren Informationen entstehen, haftet allein der Anbieter der verlinkten Webseiten.</p>
        </div>
        <div className='container mx-auto px-4 mt-6'>
        <p className='text-base'><strong>Urheberrechte und Markenrechte</strong>: Alle auf dieser Website dargestellten Inhalte, wie Texte, Fotografien, Grafiken, Marken und Warenzeichen sind durch die jeweiligen Schutzrechte (Urheberrechte, Markenrechte) geschützt. Die Verwendung, Vervielfältigung usw. unterliegen unseren Rechten oder den Rechten der jeweiligen Urheber bzw. Rechteverwalter.</p>
        </div>
        <div className='container mx-auto px-4 mt-6'>
        <p className='text-base'><strong>Hinweise auf Rechtsverstöße</strong>: Sollten Sie innerhalb unseres Internetauftritts Rechtsverstöße bemerken, bitten wir Sie uns auf diese hinzuweisen. Wir werden rechtswidrige Inhalte und Links nach Kenntnisnahme unverzüglich entfernen.</p>
        </div>
        <div className='container mx-auto px-4 mt-6 mb-6 italic text-red-400'>
        <p class="seal"><a href='https://datenschutz-generator.de/?l=de' title="Rechtstext von Dr. Schwenke - für weitere Informationen bitte anklicken." target='_blank' rel='noopener noreferrer nofollow'>Erstellt mit kostenlosem Datenschutz-Generator.de von Dr. Thomas Schwenke</a></p>      
      </div>
    </>
  );
}

export default Impressum
