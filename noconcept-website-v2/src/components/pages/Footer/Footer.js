import React from 'react';
import './Footer.css';
//import { Button } from '../../Button';
import { Link } from 'react-router-dom';
import {
  FaFacebook,
  FaInstagram,
  FaYoutube,
  FaTwitter,
  FaLinkedin,
  FaToriiGate
} from 'react-icons/fa';

function Footer() {
  return (
    <div className='footer-container'>
      <div className='footer-links'>
        <div className='footer-link-wrapper'>
          <div className='footer-link-items'>
            <h2>Rechtliches:</h2>
            <Link to='/impressum'>Impressum</Link>
            <Link to='/datenschutz'>Datenschutz</Link>
          </div>
          <div className='footer-link-items'>
            <h2>Schreiben Sie uns:</h2>
            <a href="mailto:contact@noconcept.design">Kontakt</a>
          </div>
        </div>
      </div>
      <section className='social-media'>
        <div className='social-media-wrap'>
          <div className='footer-logo'>
            <Link to='/' className='social-logo'>
              <FaToriiGate className='navbar-icon' /></Link>
          </div>
          <small className='website-rights'>NOCONCEPT Design © 2020</small>
          <div className='social-icons'>
            <Link
              className='social-icon-link'
              to='//www.facebook.com/ejo.netsroht'
              target='_blank'
              aria-label='Facebook'
            >
              <FaFacebook />
            </Link>
            <Link
              className='social-icon-link'
              to='//instagram.com/0berfoerster'
              target='_blank'
              aria-label='Instagram'
            >
              <FaInstagram />
            </Link>
            <Link
              className='social-icon-link'
              to={
                '//www.youtube.com/channel/UC5Wc9I5eQeFFvDLstagwfNg/'
              }
              target='_blank'
              aria-label='Youtube'
            >
              <FaYoutube />
            </Link>
            <Link
              className='social-icon-link'
              to='//twitter.com/0berfoerster'
              target='_blank'
              aria-label='Twitter'
            >
              <FaTwitter />
            </Link>
            <Link
              className='social-icon-link'
              to='//linkedin.com/in/thorsten-oje'
              target='_blank'
              aria-label='LinkedIn'
            >
              <FaLinkedin />
            </Link>
          </div>
        </div>
      </section>
    </div>
  );
}

/*
function Footer() {
  return (
    <div className='footer-container'>
      <section className='footer-subscription'>
        <p className='footer-subscription-heading'>
          Bestellen Sie unseren Spamletter, um zu erfahren wann unser Azubi wieder aus seinem Urlaub zurück ist.
        </p>
        <p className='footer-subscription-text'>
          Sie können den Spamletter jederzeit wieder abbestellen.
        </p>
        <div className='input-areas'>
          <form>
            <input
              className='footer-input'
              name='email'
              type='email'
              placeholder='Your Email'
            />
            <Button buttonStyle='btn--outline'>Spamletter bestellen</Button>
          </form>
        </div>
      </section>
      <div className='footer-links'>
        <div className='footer-link-wrapper'>
          <div className='footer-link-items'>
            <h2>Rechtliches</h2>
            <Link to='/impressum'>Impressum</Link>
            <Link to='/datenschutz'>Datenschutz</Link>
          </div>
          <div className='footer-link-items'>
            <h2>Schreiben Sie uns</h2>
            <Link to='/kontakt'>Kontakt</Link>
          </div>
        </div>
      </div>
      <section className='social-media'>
        <div className='social-media-wrap'>
          <div className='footer-logo'>
            <Link to='/#top' className='social-logo'>
              <FaToriiGate className='navbar-icon' /></Link>
          </div>
          <small className='website-rights'>NOCONCEPT Design © 2020</small>
          <div className='social-icons'>
            <Link
              className='social-icon-link'
              to='//facebook.com/ejo.netsroth'
              target='_blank'
              aria-label='Facebook'
            >
              <FaFacebook />
            </Link>
            <Link
              className='social-icon-link'
              to='//instagram.com/0berförster'
              target='_blank'
              aria-label='Instagram'
            >
              <FaInstagram />
            </Link>
            <Link
              className='social-icon-link'
              to={
                ''
              }
              target='_blank'
              aria-label='Youtube'
            >
              <FaYoutube />
            </Link>
            <Link
              className='social-icon-link'
              to='//twitter.com/0berfoerster'
              target='_blank'
              aria-label='Twitter'
            >
              <FaTwitter />
            </Link>
            <Link
              className='social-icon-link'
              to='//linkedin.com/in/thorsten-oje'
              target='_blank'
              aria-label='LinkedIn'
            >
              <FaLinkedin />
            </Link>
          </div>
        </div>
      </section>
    </div>
  );
}*/

export default Footer;