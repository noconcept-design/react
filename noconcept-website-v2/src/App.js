import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
//import './App.css';
import Navbar from './components/Navbar';
import Footer from './components/pages/Footer/Footer';
import Home from './components/pages/HomePage/Home';
import Impressum from './components/pages/Impressum/Impressum';
import Datenschutz from './components/pages/Datenschutz/Datenschutz';
import Skills from './components/Skills';
import Meinungen from './components/Meinungen';
import Work from './components/Work';
import GameOfLife from './components/pages/GameofLife/GameOfLife';
import Tetris from './components/pages/Tetris/Tetris';
import CatMeme from './components/pages/CatMeme/CatMeme';

function App() {
  return (
    <Router>
      <Navbar />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/skills' component={Skills} />          
          <Route path='/work' component={Work} />          
          <Route path='/gol' component={GameOfLife} />          
          <Route path='/tetris' component={Tetris} />          
          <Route path='/catmeme' component={CatMeme} />          
          <Route path='/meinungen' component={Meinungen} />          
          <Route path='/impressum' component={Impressum} />
          <Route path='/datenschutz' component={Datenschutz} />
        </Switch>
      <Footer />
    </Router>
  );
}

export default App;
