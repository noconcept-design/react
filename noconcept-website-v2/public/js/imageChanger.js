(function() {
    var rotator = document.getElementById('imgRotate'), //get the element
        dir = 'images/kate/',                              //images folder
        delayInSeconds = 1.0,                           //delay in seconds
        num = 0,                                      //start number
        len = 3;                                      //limit
    setInterval(function(){                           //interval changer
        rotator.src = dir + num+'.png';               //change picture
        num = (num === len) ? 0 : ++num;              //reset if last image reached
    }, delayInSeconds * 50);
}());