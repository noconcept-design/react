import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import Header from './Components/Header'
import Footer from './Components/Footer'
import Home from './Views/Home'
import Article from './Views/Article'
import About from './Views/About'
import Impressum from './Views/Impressum'
import Datenschutz from './Views/Datenschutz'
import Kontakt from './Views/Kontakt'



function App() {
  return (
    <div className="relative pb-10 min-h-screen">

      <Router>   
        <Header />
        <div className="p-3">   
          <Switch>
            <Route exact path="/">
              <Home />            
            </Route>
            <Route path="/articles/:id">
              <Article />
            </Route>           
            <Route path="/about">
              <About />            
            </Route>
            <Route path="/impressum">
              <Impressum />
            </Route>
            <Route path="datenschutz">
              <Datenschutz />
            </Route>
            <Route path="/kontakt">
              <Kontakt />
            </Route>        
          </Switch>
        </div>

        <Footer />
      </Router>
    </div>
  );
}

export default App;
