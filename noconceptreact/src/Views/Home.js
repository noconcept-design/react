import React from 'react';
import Loader from '../Components/Loader'
import ArticleCard from '../Components/ArticleCard'
import { useAxiosGet } from '../Hooks/HttpRequests'
import ScrollToTop from '../Components/ScrollToTop'

function Home(){
  const url = `https://5f582d4c1a07d600167e78d6.mockapi.io/api/v1/articles?page=1&limit=10` 


  let articles = useAxiosGet(url)
  let content = null

  if(articles.error){
    content = <div>
      <div className="bg-red-300 p-3">Oopps .. Fehler bitte dem Browser einen Schluck Wasser gehen um in damit zu refreshen, oder versuchen Sie es später nochmal einmal.
      </div>
    </div>
  }

  if(articles.loading){
    content = <Loader></Loader>
  }
  
  if(articles.data){
    content = 
    articles.data.map((article) =>
      <div key={article.id} className="flex-no-shrink w-full md:w1/4 md:px-3">
        <ArticleCard 
          article={article}
        />
      </div>
    )
  }

  return(
    <div className="container mx-auto">
      <h1 className="font-bold text-2xl mb-3">
        Startseite
        </h1>
        <div className="md:flex flex-wrap md:-mx-3">
          {content}
          <ScrollToTop></ScrollToTop>
        </div>    
    </div>

  )
}


export default Home

