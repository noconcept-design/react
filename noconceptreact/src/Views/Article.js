import React from 'react';
import { useParams } from 'react-router-dom'
import { useAxiosGet } from '../Hooks/HttpRequests'
import Loader from '../Components/Loader'


function Article(){
  const { id } = useParams()
  const url = `https://5e9623dc5b19f10016b5e31f.mockapi.io/api/v1/products/${id}` 

  let article = useAxiosGet(url)
  let content = null

  if(article.error){
    content = <p>Oopps .. Fehler bitte dem Browser einen Schluck Wasser gehen um in damit zu refreshen, oder versuchen Sie es später nochmal einmal.</p>
  }

  if(article.loading){
    content = <Loader></Loader>
  }

  if(article.data){
    content = 
    <div>
      <h1 className="text-2xl font-bold mb-3">
        {article.data.name}
      </h1>
      <div>
        <img
          src={article.data.images[1].imageUrl}
          alt={article.data.name}
          />
      </div>
      <div className="font-bold text-xl mb-3">
        {article.data.content}
      </div>
      <div>
        {article.data.description}
      </div>
    </div>
  }

  return(
    <div className="container mx-auto">
      { content }
    </div>
  )
}

export default Article