import React from 'react';

function Impressum(){
  return(
    <div>
      <h1 className="font-bold text-2xl mb-3">
        Angaben gemäß § 5 TMG:
      </h1>
      <address>
        <p>Max Mustermann</p>
        <p>Im Brühl 19</p>
       
        <p>12345 Buxdehude</p>

        <p>Tel.: 0123 45678</p>
        <p>E-Mail: <a className ="text-blue-500" href="mailto:info@example.com">info@example.com</a> 
        </p>   
      </address>  
    </div>
  )
}


export default Impressum