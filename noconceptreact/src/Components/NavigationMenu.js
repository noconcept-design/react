import React from 'react'
import { Link } from 'react-router-dom'

function NavigationMenu(props){
  return (
    <div>
      <div className="font-bold py-3">
        Menü
      </div>
      <ul>
        <li>
          <Link
            to="/" 
            className="text-blue-500 py-3 border-t border-b block"
            onClick={props.closeMenu}
          >
              Home
          </Link>
        </li>
        <li>
          <Link 
            to="/about"
            className="text-blue-500 py-3 border-b block"
            onClick={props.closeMenu}
          >
              Über
          </Link>
        </li>
        <li>
          <Link 
            to="/impressum" 
            className="text-blue-500 py-3 border-b block"
            onClick={props.closeMenu}
          >
              Impressum
          </Link>
        </li>
        <li>
          <Link  
            to="/datenschutz"
            className="text-blue-500 py-3 border-b block"
            onClick={props.closeMenu}
          >
              Datenschutz
          </Link>
        </li>
        <li>
          <Link 
            to="/kontakt"
            className="text-blue-500 py-3 border-b block"
            onClick={props.closeMenu}
          >
              Kontakt
          </Link>
        </li>
      </ul>       
    </div>
  )
}

export default NavigationMenu