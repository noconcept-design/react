import React from 'react';
// index.css
function Loader(){
  return(
    <div className="flex justify-center">
      <div className="loader"></div>
    </div>
  )
}

export default Loader