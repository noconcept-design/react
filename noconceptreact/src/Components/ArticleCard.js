import React from 'react'
import { Link } from 'react-router-dom'

function ArticleCard(props){
  return(
    <div className="border mb-4 rounded overflow-hidden">
      <Link to={`/articles/${props.article.id}`}>
        <div
          style={{ 
            'backgroundImage': `url('${props.article.images[1].imageUrl}')`,          
          }}
          className="w-full h-64 bg-blue bg-cover"
        >
        </div>        
      </Link>
      <div className="p-3">
        <h3 className="font-bold text-xl mb-3">
          <Link to={`/articles/${props.article.id}`}>
            { props.article.name }
          </Link>
        </h3>
        <div className="font-bold mb-3">
          {props.article.description}
        </div>
        <Link to={`/articles/${props.article.id}`}
          className="bg-blue-500 text-white p-2 flex justify-center"
        >
          Lesen
        </Link>
      
      </div>


    </div>
  )
}

export default ArticleCard